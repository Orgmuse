# Copyright (C) 2004  Alex Schroeder <alex@emacswiki.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the
#    Free Software Foundation, Inc.
#    59 Temple Place, Suite 330
#    Boston, MA 02111-1307 USA

$ModulesDescription .= '<p><a href="http://git.savannah.gnu.org/cgit/oddmuse.git/tree/modules/html-uploads.pl">html-uploads.pl</a>, see <a href="http://www.oddmuse.org/cgi-bin/oddmuse/Restricted_HTML_Upload">Restricted HTML Upload</a></p>';

$Action{download} = \&HtmlUploadsDoDownload;

# anybody can download raw html

sub HtmlUploadsDoDownload {
  push(@UploadTypes, 'text/html') unless grep(/^text\/html$/, @UploadTypes);
  return DoDownload(@_);
}

# but only admins can upload raw html

*OldHtmlUploadsDoPost = *DoPost;
*DoPost = *NewHtmlUploadsDoPost;

sub NewHtmlUploadsDoPost {
  my @args = @_;
  if (not grep(/^text\/html$/, @UploadTypes)
      and UserIsAdmin()) {
    push(@UploadTypes, 'text/html');
  }
  return OldHtmlUploadsDoPost(@args);
}
