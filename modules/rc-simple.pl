# Copyright (C) 2004  Tilmann Holst
# Copyright (C) 2004, 2005, 2007  Alex Schroeder <alex@emacswiki.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

$ModulesDescription .= '<p><a href="http://git.savannah.gnu.org/cgit/oddmuse.git/tree/modules/rc-simple.pl">rc-simple.pl</a>, see <a href="http://www.oddmuse.org/cgi-bin/oddmuse/Rc_Simple_Extension">Rc Simple Extension</a></p>';

sub RcSimpleHtml {
  my $maxEntries = shift;
  my @html = ();
  # Optimize param fetches and translations out of main loop
  my $all = GetParam('all', 0);
  my $printRCLine = sub {
    my($id, $ts, $host, $username, $summary, $minor, $revision,
       $languages, $cluster, $last) = @_;
    my $all_revision = $last ? undef : $revision; # no revision for the last one
    my $pagelink = '';
    if ($all) {
      $pagelink = GetOldPageLink('browse', $id, $all_revision, $id, $cluster);
    } elsif ($cluster) {
      $pagelink = GetOldPageLink('browse', $id, $revision, $id, $cluster);
    } else {
      $pagelink = GetPageLink($id, $cluster);
    }
    push (@html, $pagelink);
  };
  ProcessRcLines(sub {}, $printRCLine);
  my $to = GetParam('from', $Now - GetParam('days', $RcDefault) * 86400);
  my $from = $to - GetParam('days', $RcDefault) * 86400;
  my $more = "action=rc;from=$from;upto=$to";
  foreach (qw(all showedit rollback rcidonly rcuseronly rchostonly
	      rcclusteronly rcfilteronly match lang followup)) {
    my $val = GetParam($_, '');
    $more .= ";$_=$val" if $val;
  }
  push(@html, $q->p({-class=>'more'}, ScriptLink($more, T('More...'), 'more')));
  return (@html <= $maxEntries) ? @html : @html[0..$maxEntries-1];
}

push(@MyRules, \&RcSimpleRule);

sub RcSimpleRule {
  if (m/\G(\&lt;rc(:(\d+))?\&gt;)/gci) {
    my $maxEntries = $3;
    return unless $maxEntries;
    return if ($maxEntries <= 0);
    my @recentPages = RcSimpleHtml($maxEntries);
    Clean(CloseHtmlEnvironments());
    Dirty($1);
    print $q->div({-class=>'rc'}, $q->span(\@recentPages));
    return AddHtmlEnvironment('p');
  }
  return undef;
}
