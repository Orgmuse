#!/usr/bin/env perl
# ====================[ new.pl                          ]====================

=head1 NAME

new - An Oddmuse module for creating new pages

=head1 INSTALLATION

new is easily installable; move this file into the B<wiki/modules/>
directory for your Oddmuse Wiki.

=head1 DESCRIPTION

This module allows you to create new pages without any hassles.  You
no longer have to create dead links and follow them to create new
pages.  Use this module if your wiki uses large number of standalone
pages.

Look out for `New' links added to the `Administration' page and the
Goto Bar.  Clicking on the `New' 'link will take you to a form that is
filled with today's date.  If you submit this form, a new page with
today's date will be created.  If you would rather prefer to create a
page with a different name, provide a different `Title'.  The pagename
that you fill in I<need not> be a fully new page.  It can be I<any>
existing page.  If a page with the name that you typed already exists
you will be offered to edit the current revision of the page.  Don't
be afraid that content on existing page will be erased.  Feel
adventurous!

=cut

package OddMuse;

$ModulesDescription .= '<p><a href="http://git.savannah.gnu.org/cgit/oddmuse.git/tree/modules/new.pl">new.pl</a>, see <a href="http://www.oddmuse.org/cgi-bin/oddmuse/Create_New_Page">New Page Extension</a></p>';

push(@MyInitVariables, sub {
       $EditorGotoBar .= ScriptLink('action=new', T('New'), 'new');
});

push(@MyAdminCode, \&NewMenu);

sub NewMenu {
  my ($id, $menuref, $restref) = @_;
  push(@$menuref, ScriptLink('action=new', T('New'), 'new'));
}

# New Action
$Action{new} = \&DoNewPage;

=head1 GetNewPageName

Return the default page name for new pages.  Override this function to
choose a different default.

Use C<CalcDay> to return today's date in YYYY-MM-DD format.  If you
have localtime.pl loaded, the date will match your local timezone.
Otherwise, it will be in UTC.

=cut

sub GetNewPageName {
  return CalcDay($Now);		# Use `CalcDay' and not gmtime.  This
				# ensures that if localtime.pl is
				# loaded, then dates of new pages will
				# match user expectations.
}

sub DoNewPage {
  my $id = GetParam('id', '');
  if ($id) {
    $id = UrlEncode(FreeToNormal($id)); # FIXME: Is this correct?
    return DoEdit($id);
  } else {
    # FIXME: Use a new class or just stick with what weblog-4.pl uses...
    print GetHeader('', T('New')), $q->start_div({-class=>'content categories'}),
      GetFormStart(undef, 'get', 'cat');
    my $go = T('Go!');
    $id = GetNewPageName();
    print $q->p(T('Title: '),
		qq{<input type="text" name="id" value="$id" tabindex="1" />},
		GetHiddenValue('action', 'new'));
    print $q->p(qq{<input type="submit" value="$go" tabindex="2" />});
    print $q->end_form, $q->end_div();
    PrintFooter();
  }
}

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2006 Alex Schroeder <alex@emacswiki.org>
              2013 Jambunathan <kjambunathan at gmail dot com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the
   Free Software Foundation, Inc.
   59 Temple Place, Suite 330
   Boston, MA 02111-1307 USA
