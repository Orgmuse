#!/usr/bin/env perl
# ====================[ syntax-highlight.pl                          ]====================

=head1 NAME

syntax-highlight - An Oddmuse module for syntax highlighting of source
snippets.

=head1 INSTALLATION

syntax-highlight is easily installable; move this file into the
B<wiki/modules/> directory for your Oddmuse Wiki.

=head1 DEPENDENCIES

This module relies on L<Syntax::Highlight::Engine::Kate> to do the
actual coloring.  On a Debian, the Kate syntax highlighter is
available as package C<libsyntax-highlight-engine-kate-perl>.

=cut
package OddMuse;

$ModulesDescription .= '<p><a href="http://git.savannah.gnu.org/cgit/oddmuse.git/tree/modules/syntax-highlight.pl">syntax-highlight.pl</a>, see <a href="http://www.oddmuse.org/wiki/Syntax_Highlighting">Syntax Highlight Extension</a></p>';

use Syntax::Highlight::Engine::Kate;

=head1 VARIABLES

=head2 %NativeLanguageToKateLanguage

Languages supported by the Kate syntax highlighter.  Use the value of
this variable as a reference to setup language mapper for your markup
module.  See C<HighlightSyntax>.

=cut

my %NativeLanguageToKateLanguage =
  (
   ".desktop"		 => ".desktop",
   "4GL"		 => "4GL",
   "4GL-PER"		 => "4GL-PER",
   "ABC"		 => "ABC",
   "AHDL"		 => "AHDL",
   "ANSI C89"		 => "ANSI C89",
   "ASP"		 => "ASP",
   "AVR Assembler"	 => "AVR Assembler",
   "AWK"		 => "AWK",
   "Ada"		 => "Ada",
   "Alerts"		 => "Alerts",
   "Ansys"		 => "Ansys",
   "Apache Configuration"=> "Apache Configuration",
   "Asm6502"		 => "Asm6502",
   "Bash"		 => "Bash",
   "BibTeX"		 => "BibTeX",
   "C"			 => "C",
   "C#"			 => "C#",
   "C++"		 => "C++",
   "CGiS"		 => "CGiS",
   "CMake"		 => "CMake",
   "CSS"		 => "CSS",
   "CSS/PHP"		 => "CSS/PHP",
   "CUE Sheet"		 => "CUE Sheet",
   "Cg"			 => "Cg",
   "ChangeLog"		 => "ChangeLog",
   "Cisco"		 => "Cisco",
   "Clipper"		 => "Clipper",
   "ColdFusion"		 => "ColdFusion",
   "Common Lisp"	 => "Common Lisp",
   "Component-Pascal"	 => "Component-Pascal",
   "D"			 => "D",
   "Debian Changelog"	 => "Debian Changelog",
   "Debian Control"	 => "Debian Control",
   "Diff"		 => "Diff",
   "Doxygen"		 => "Doxygen",
   "E Language"		 => "E Language",
   "Eiffel"		 => "Eiffel",
   "Email"		 => "Email",
   "Euphoria"		 => "Euphoria",
   "Fortran"		 => "Fortran",
   "FreeBASIC"		 => "FreeBASIC",
   "GDL"		 => "GDL",
   "GLSL"		 => "GLSL",
   "GNU Assembler"	 => "GNU Assembler",
   "GNU Gettext"	 => "GNU Gettext",
   "HTML"		 => "HTML",
   "Haskell"		 => "Haskell",
   "IDL"		 => "IDL",
   "ILERPG"		 => "ILERPG",
   "INI Files"		 => "INI Files",
   "Inform"		 => "Inform",
   "Intel x86 (NASM)"	 => "Intel x86 (NASM)",
   "JSP"		 => "JSP",
   "Java"		 => "Java",
   "JavaScript"		 => "JavaScript",
   "JavaScript/PHP"	 => "JavaScript/PHP",
   "Javadoc"		 => "Javadoc",
   "KBasic"		 => "KBasic",
   "Kate File Template"	 => "Kate File Template",
   "LDIF"		 => "LDIF",
   "LPC"		 => "LPC",
   "LaTeX"		 => "LaTeX",
   "Lex/Flex"		 => "Lex/Flex",
   "LilyPond"		 => "LilyPond",
   "Literate Haskell"	 => "Literate Haskell",
   "Logtalk"		 => "Logtalk",
   "Lua"		 => "Lua",
   "M3U"		 => "M3U",
   "MAB-DB"		 => "MAB-DB",
   "MIPS Assembler"	 => "MIPS Assembler",
   "Makefile"		 => "Makefile",
   "Mason"		 => "Mason",
   "Matlab"		 => "Matlab",
   "Modula-2"		 => "Modula-2",
   "Music Publisher"	 => "Music Publisher",
   "Objective Caml"	 => "Objective Caml",
   "Objective-C"	 => "Objective-C",
   "Octave"		 => "Octave",
   "PHP (HTML)"		 => "PHP (HTML)",
   "PHP/PHP"		 => "PHP/PHP",
   "POV-Ray"		 => "POV-Ray",
   "Pascal"		 => "Pascal",
   "Perl"		 => "Perl",
   "PicAsm"		 => "PicAsm",
   "Pike"		 => "Pike",
   "PostScript"		 => "PostScript",
   "Prolog"		 => "Prolog",
   "PureBasic"		 => "PureBasic",
   "Python"		 => "Python",
   "Quake Script"	 => "Quake Script",
   "R Script"		 => "R Script",
   "REXX"		 => "REXX",
   "RPM Spec"		 => "RPM Spec",
   "RSI IDL"		 => "RSI IDL",
   "RenderMan RIB"	 => "RenderMan RIB",
   "Ruby"		 => "Ruby",
   "SGML"		 => "SGML",
   "SML"		 => "SML",
   "SQL (MySQL)"	 => "SQL (MySQL)",
   "SQL (PostgreSQL)"	 => "SQL (PostgreSQL)",
   "SQL"		 => "SQL",
   "Sather"		 => "Sather",
   "Scheme"		 => "Scheme",
   "Sieve"		 => "Sieve",
   "Spice"		 => "Spice",
   "Stata"		 => "Stata",
   "TI Basic"		 => "TI Basic",
   "TaskJuggler"	 => "TaskJuggler",
   "Tcl/Tk"		 => "Tcl/Tk",
   "UnrealScript"	 => "UnrealScript",
   "VHDL"		 => "VHDL",
   "VRML"		 => "VRML",
   "Velocity"		 => "Velocity",
   "Verilog"		 => "Verilog",
   "WINE Config"	 => "WINE Config",
   "Wikimedia"		 => "Wikimedia",
   "XML"		 => "XML",
   "Yacc/Bison"		 => "Yacc/Bison",
   "de_DE"		 => "de_DE",
   "en_US"		 => "en_US",
   "ferite"		 => "ferite",
   "nl"			 => "nl",
   "progress"		 => "progress",
   "scilab"		 => "scilab",
   "txt2tags"		 => "txt2tags",
   "x.org Configuration" => "x.org Configuration",
   "xHarbour"		 => "xHarbour",
   "xslt"		 => "xslt",
   "yacas"		 => "yacas"
  );

my $syntax_hl =
  new Syntax::Highlight::Engine::Kate
  (language => 'Perl',
   substitutions =>
   {
    "<" => "&lt;",
    ">" => "&gt;",
    "&" => "&amp;",
    # Oddmuse encloses src blocks within <pre>...</pre>.
    # " " => "&nbsp;",
    # "\t" => "&nbsp;&nbsp;&nbsp;",
    # "\n" => "<br/>\n",
   },
   format_table =>
   {
    Alert	=> ["<span style=\"color: #0000ff\">"		 , "</span>"],
    BaseN	=> ["<span style=\"color: #007f00\">"		 , "</span>"],
    # BString	=> ["<span style=\"color: #c9a7ff\">"		 , "</span>"],
    BString	=> [""						 , ""],
    Char	=> ["<span style=\"color: #ff00ff\">"		 , "</span>"],
    Comment	=> ["<span style=\"color: #7f7f7f\"><em>"	 , "</em></span>"],
    DataType	=> ["<span style=\"color: #0000ff\">"		 , "</span>"],
    DecVal	=> ["<span style=\"color: #00007f\">"		 , "</span>"],
    Error	=> ["<span style=\"color: #ff0000\"><strong><em>", "</em></strong></span>"],
    Float	=> ["<span style=\"color: #00007f\">"		 , "</span>"],
    Function	=> ["<span style=\"color: #007f00\">"		 , "</span>"],
    IString	=> ["<span style=\"color: #ff0000\">"		 , "<//span>"],
    Keyword	=> ["<strong>"					 , "</strong>"],
    Normal	=> [""						 , ""],
    Operator	=> ["<span style=\"color: #ffa500\">"		 , "</span>"],
    Others	=> ["<span style=\"color: #b03060\">"		 , "</span>"],
    RegionMarker=> ["<span style=\"color: #96b9ff\"><em>"	 , "</em></span>"],
    Reserved	=> ["<span style=\"color: #9b30ff\"><strong>"	 , "</strong></span>"],
    String	=> ["<span style=\"color: #ff0000\">"		 , "</span>"],
    Variable	=> ["<span style=\"color: #0000ff\"><strong>"	 , "</strong></span>"],
    Warning	=> ["<span style=\"color: #0000ff\"><strong><em>", "</strong></em></span>"],
   },
  );

=head1 FUNCTIONS

=head2 HighlightSyntax($src, $kateLang)

Markup C<$quoted_src> in C<$kateLang> and return the resulting HTML.
If C<$kateLang> is unknown to Kate, return C<$quoted_src>.  Assume
C<$quoted_src> as quoted.  Unquote the source block with
C<UnquoteHtml> before passing it to Kate for highlighting.

Before using this function, you may have to setup up a hash that maps
from the name of the language as it is designated by your markup to
the how the language itself is called by Kate.  Use
C<%NativeLanguageToKateLanguage> as a template to setup this hash.

Invoke C<HighlightSyntax> as below.

	my %OrgLanguageToKateLanguage = ("perl"=>"Perl");
	# Do Syntax Highlighting
	if (defined(&HighlightSyntax)) {
	  $lines = HighlightSyntax($src, $OrgLanguageToKateLanguage{$lang});
	}

Here, the hash C<%OrgLanguageToKateLanguage> maps the language from
Emacs/Org-mode to Kate.  As seen above, C<Perl> blocks in
Emacs/Org-mode markup are designated by C<perl> while in Kate they are
designated by C<Perl>.

=cut

sub HighlightSyntax {
  my ($quoted_src, $kateLang) = @_;

  # No highlighter for current language
  return $quoted_src
    unless $syntax_hl->languagePlug($kateLang);

  # Use highlighter
  my $unquoted_src = UnquoteHtml($quoted_src);
  $syntax_hl->language($kateLang);
  return $syntax_hl->highlightText($unquoted_src);
}

=head1 COPYRIGHT AND LICENSE

Copyright 2013       by Jambunathan K <kjambunathan@gmail.com>.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see L<http://www.gnu.org/licenses/>.

=cut
