/* bookmarklet.js --- Capture Webpage URLs to Oddmuse Wiki. */

/*

Copyright (C) 2013 Jambunathan K <kjambunathan at gmail dot com>

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

*/

/* COMMENTARY

While visiting a webpage, use this bookmarklet to capture the page's
URL and Title in to an Oddmuse Wiki page.  In additon to URL and Title
you can also snip portions of text off the webpage.

For original inspiration and discussion surrouding this feature, see
the following pages

    - http://oddmuse.org/wiki/Clipping_Bookmarklet
    - http://oddmuse.org/wiki/Comments_on_Clipping_Bookmarklet

Configure the Bookmarklet for your specific setup
=================================================

- wikiBase      :: Base URL for the Wiki.
     Default value: `http://localhost/cgi-bin/wiki.pl’

- scrapPage     :: Oddmuse Wiki page to which the captured text is
     dumped.

     Default value: ‘Scraps’

- autoSubmit :: Set it to false, if you want to review either the
     captured text or the Wiki pagename to which the text is dumped.
     The value `false' 'is what you want, if

     - you want to note the "context" for your capture

     - you need to modify the Wiki pagename to which the text is dumped

     - you are capturing code snippets off webpages and want to
       surround the captured script with `{{{' '}}}' markers (or
       whatever are the default markers for pre-formatted text).

     Default value: false.

- autocloseTimeout :: Timeout (in milliseconds) after which the
     "pop-up" window opened by the Bookmarklet will close.

     Default value: 5000 ms.

- wikiTextSpec          :: Syntax of captured text.

    Default value: '\n----\n[%U %T]\n\n%H'

     The %-specifiers will be replaced as follows.
     - %U ⇒ URL of Webpage.
     - %T ⇒ Title of Webpage.
     - %H ⇒ Highlighted (or Selected) text.

    Note: Some webpages have no Titles. So, you may want to account
     for this possibility.  See the script for more information.

    If you are using Oddmuse with Emacs Org-mode markup, then set the
     above variable to

     var wikiTextSpec = stash.T ? '\n----\n[[%U][%T]]\n\n%H'
             :  '\n----\n[[%U]]\n\n%H';

Create a Bookmarklet in your Web Browser
========================================

If you are using Iceweasel, you can use the following steps.

1. View -> Toolbars -> Bookmarks Toobar -> Enable it.
2. Right-click on the Bookmarks Toolbar -> New Bookmark
   1. `Name' => Choose a suitable name (say "Submit to Oddmuse Wiki")
   2. `Location' => Copy paste the below function
3. Click on `Add' to finish.

Capture URLs and snippets of text from Webpages
===============================================

1. Visit your favourite Webpage: http://oddmuse.org.
2. Highlight some text within the above page.
3. Click on the new Bookmarklet you created.  An Oddmuse submit form
   will pop-up and close after sometime.
4. Check RecentPages on your Wiki and confirm that everything is OK.
5. See previous section for options that are made available to you.
*/

/* CODE */

javascript:void((function() {
    var wikiBase = 'http://localhost/cgi-bin/wiki.pl';
    var scrapPage = 'Scraps';
    var autoSubmit = false;
    var autocloseTimeout = 5000;
    var stash = new Object;
    stash.U = document.location;
    stash.T = document.title;
    stash.H = window.getSelection();
    var wikiTextSpec = stash.T ? '\n----\n[%U %T]\n\n%H'
	:  '\n----\n[%U]\n\n%H';
    var wikiText = wikiTextSpec.replace(/%([UTH])/g, function(m, p) {return (stash[p] || '');});
    var nw = window.open('','','toolbar=0,resizable=1,scrollbars=1,status=1,width=750,height=500');
    var nwd = nw.document;

    nwd.body.innerHTML =
	'<div class="comment">' +
	'  <form method="post" action="http://localhost/cgi-bin/wiki.pl/Public" enctype="multipart/form-data" accept-charset="utf-8" class="comment">' +
	'    <p>' +
	'      <label for="title">Title:</label>' +
	'      <input name="title" id="title">' +
	'      </input>' +
	'      <br/>' +
	'      <textarea name="aftertext" rows="10" cols="78" id="aftertext">' +
	'      </textarea>' +
	'    </p>' +
	'    <p>' +
	'      <span class="username">' +
	'    	<label for="username">Username:</label>' +
	'    	<input name="username" size="20" maxlength="50" id="username" type="text">' +
	'	</input>' +
	'    	</span>' +
	'    	<br/>' +
	'    	<span class="homepage">' +
	'    	  <label for="homepage">Homepage URL:</label>' +
	'    	  <input name="homepage" size="40" maxlength="100" id="homepage" type="text">' +
	'	  </input>' +
	'    	</span>' +
	'    </p>' +
	'    <p>' +
	'      <input name="Save" value="Save" accesskey="s" type="submit">' +
	'      </input>' +
	'      <input name="Preview" value="Preview" accesskey="p" type="submit">' +
	'      </input>' +
	'    </p>' +
	'  </form>' +
	'</div>';

    nwd.getElementById('title').setAttribute('value', scrapPage);
    nwd.getElementById('aftertext').value = wikiText;

    var f = nwd.forms[0];
    f.setAttribute('action', wikiBase);
    f.onsubmit = function () {self.setTimeout(function () {nw.close ();}, autocloseTimeout);};

    if (autoSubmit) {
	f.submit ();
    }

})())
