;;; orgmuse.el --- Edit and post to Oddmuse Wikis

;; Copyright (C) 2013-2014 Jambunathan K <kjambunathan at gmail dot com>

;; Author: Jambunathan K <kjambunathan at gmail dot com>

;;; Commentary:

;;; Code:

(require 'mm-url)
(require 'url-http)

(defvar orgmuse-wikiname nil)
(defvar orgmuse-pagename nil)

(make-variable-buffer-local 'orgmuse-wikiname)
(make-variable-buffer-local 'orgmuse-pagename)

(defgroup orgmuse nil
  "Edit or Post Oddmuse wikis."
  :group 'external)

(defcustom orgmuse-wikis
  `(("localhost"
     :url "http://localhost/cgi-bin/wiki.pl/"
     :username ,(or user-full-name "Anonymous")))
  "List of Orgmuse wikis."
  :type '(repeat (cons (string :tag "Wiki Name")
		       (plist :options (:url :user-name :password))))
  :group 'orgmuse)

(defmacro orgmuse--with-work-buffer (url &rest body)
  "Run BODY in a buffer containing the contents of FILE at LOCATION.
LOCATION is the base location of a package archive, and should be
one of the URLs (or file names) specified in `package-archives'.
FILE is the name of a file relative to that base location.

This macro retrieves FILE from LOCATION into a temporary buffer,
and evaluates BODY while that buffer is current.  This work
buffer is killed afterwards.  Return the last value in BODY."
  (declare (indent 2) (debug t))
  `(let* ((buffer (url-retrieve-synchronously ,url)))
     (prog1
	 (with-current-buffer buffer
	   (progn (let ((response (url-http-parse-response)))
		    (when (or (< response 200) (>= response 300))
		      (error "Error during download request: %s"
			     (buffer-substring-no-properties (point) (line-end-position)))))
		  (set-buffer-multibyte t)
		  (decode-coding-region (point) (point-max) 'utf-8)
		  (re-search-forward "^$" nil 'move)
		  (forward-char)
		  (delete-region (point-min) (point)))
	   (buffer-string)
	   ,@body)
       (kill-buffer buffer))))

(defun orgmuse-get-all-pages (&optional wikiname)
  (interactive (list (completing-read "Wikiname: "  (mapcar 'car orgmuse-wikis)
				      nil t (caar orgmuse-wikis))))
  (let* ((wiki-plist (assoc-default wikiname orgmuse-wikis))
	 (wiki-base-url (plist-get wiki-plist :url))
	 ;; http://www.oddmuse.org/cgi-bin/oddmuse/Index_Action
	 (values '(("action" . "index")
		   ("raw" . "1")
		   ("permanentanchors" . "0"))))
    (unless wiki-base-url (user-error (format "No such wiki (%s)" wikiname)))
    (orgmuse--with-work-buffer
	(concat wiki-base-url "?" (mm-url-encode-www-form-urlencoded values))
	(split-string (buffer-string) "\n"))))

;;;###autoload
(defun orgmuse-edit (&optional wikiname pagename)
  (interactive
   (let* ((wikiname (completing-read "Wikiname: "  (mapcar 'car orgmuse-wikis)
				     nil t (caar orgmuse-wikis)))
	  (pagename (completing-read  "Page: "  (orgmuse-get-all-pages wikiname))))
     (list wikiname pagename)))
  (let* ((pagenames (orgmuse-get-all-pages wikiname))
	 (wiki-plist (assoc-default wikiname orgmuse-wikis))
	 (wiki-base-url (plist-get wiki-plist :url))
	 (values `(("action" . "browse")
		   ("raw" . "1")
		   ("id" . ,pagename))))
    (unless wiki-base-url (user-error (format "No such wiki (%s)" wikiname)))
    (let* ((text (if (member pagename pagenames)
		     (orgmuse--with-work-buffer
			 (concat wiki-base-url "?" (mm-url-encode-www-form-urlencoded values)))
		   (prog1 nil (minibuffer-message "Page %s is new" pagename)))))
      (with-current-buffer (get-buffer-create (format "%s:%s" wikiname pagename))
	(orgmuse-mode)
	(setq orgmuse-wikiname wikiname orgmuse-pagename pagename)
	(erase-buffer)
	(when text (insert text))
	(goto-char (point-min))
	(pop-to-buffer (current-buffer))))))

;;;###autoload
(defun orgmuse-post ()
  (interactive)
  ;; (unless (and (boundp 'orgmuse-mode) orgmuse-mode) (user-error "Not a Orgmuse buffer"))
  (let* ((wiki-plist (assoc-default orgmuse-wikiname orgmuse-wikis))
	 (wiki-base-url (or (plist-get wiki-plist :url)
			    (user-error (format "No such wiki (%s)" orgmuse-wikiname))))
	 (values `(("title" . ,orgmuse-pagename)
		   ("summary" . "Some updates")
		   ("username" . ,(plist-get wiki-plist :username))
		   ("pwd" . ,(plist-get wiki-plist :password))
		   ("recent_edit" . "off")
		   ("text" . ,(buffer-string)))))
    (let ((url-request-method "POST")
	  (url-request-extra-headers
	   '(("Content-Type" . "application/x-www-form-urlencoded")))
	  (url-request-data (mm-url-encode-www-form-urlencoded values)))
      (url-retrieve-synchronously wiki-base-url)
      (set-buffer-modified-p nil)
      (when (y-or-n-p "Page updated. Kill buffer?")
	(kill-buffer)))))

(define-derived-mode orgmuse-mode org-mode "Orgmuse"
  "Minor mode for editing Orgmuse pages."

  )

(provide 'orgmuse.el)
